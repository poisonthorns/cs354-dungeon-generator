#ifndef DUNGEONGRAPH_H
#define DUNGEONGRAPH_H

#include <iostream>
#include "../Rooms/Room.h"
using namespace std;

// a structure to represent a weighted edge in graph  
class Edge
{
	public:
		int src, dest;
		float weight;
	
		bool operator < (Edge const &rhs)
		{
			return weight < rhs.weight;
		}
	
};

// a structure to represent a connected, undirected  
// and weighted graph  
class Graph
{
public:
    // V-> Number of vertices, E-> Number of edges  
    int V, E;

    // graph is represented as an vector of edges.  
    // Since the graph is undirected, the edge  
    // from src to dest is also edge from dest  
    // to src. Both are counted as 1 edge here.  
    vector<Edge> edge;
};

// Creates a graph with V vertices and E edges  
Graph* createGraph(int V, int E);

// A structure to represent a subset for union-find  
class subset
{
public:
    int parent;
    int rank;
};

// A utility function to find set of an element i  
// (uses path compression technique)  
int find(subset subsets[], int i);

// A function that does union of two sets of x and y  
// (uses union by rank)  
void Union(subset subsets[], int x, int y);

// Compare two edges according to their weights.  
// Used in qsort() for sorting an array of edges  
int myComp(const void* a, const void* b);

// The main function to construct MST using Kruskal's algorithm  
vector<pair<Room, Room> > KruskalMST(Graph* graph, vector<Room> rooms);

// Driver code 
vector<pair<Room, Room> > buildMST(vector<Room> rooms);

void merge(vector<Edge> arr, int l, int m, int r);

/* l is for left index and r is right index of the
   sub-array of arr to be sorted */
void mergeSort(vector<Edge> arr, int l, int r);

#endif
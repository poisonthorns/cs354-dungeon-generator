#include "Dice.h"
#include <ctime>
#include <random>
#include <math.h>
#include <stdlib.h>

#include "../Config.h"
using namespace std;

int genRoomSize(default_random_engine &generator)
{
    if(MIN_ROOM_SIZE == MAX_ROOM_SIZE)
        return MIN_ROOM_SIZE;
    double mean = (MIN_ROOM_SIZE + MAX_ROOM_SIZE) / 2.0;
    double sigma = (MAX_ROOM_SIZE - mean) / 3.0;
	normal_distribution<double> distribution(mean, sigma);
    return (int)round(distribution(generator));
}

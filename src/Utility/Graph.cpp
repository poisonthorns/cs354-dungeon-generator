#include "Graph.h"
#include <algorithm>

using namespace std;

// Creates a graph with V vertices and E edges  
Graph* createGraph(int V, int E)
{
    Graph* graph = new Graph;
    graph->V = V;
    graph->E = E;

    return graph;
}

// A utility function to find set of an element i  
// (uses path compression technique)  
int find(subset subsets[], int i)
{
    // find root and make root as parent of i  
    // (path compression)  
    if (subsets[i].parent != i)
        subsets[i].parent = find(subsets, subsets[i].parent);

    return subsets[i].parent;
}

// A function that does union of two sets of x and y  
// (uses union by rank)  
void Union(subset subsets[], int x, int y)
{
    int xroot = find(subsets, x);
    int yroot = find(subsets, y);

    // Attach smaller rank tree under root of high  
    // rank tree (Union by Rank)  
    if (subsets[xroot].rank < subsets[yroot].rank)
        subsets[xroot].parent = yroot;
    else if (subsets[xroot].rank > subsets[yroot].rank)
        subsets[yroot].parent = xroot;

    // If ranks are same, then make one as root and  
    // increment its rank by one  
    else
    {
        subsets[yroot].parent = xroot;
        subsets[xroot].rank++;
    }
}

// Compare two edges according to their weights.  
// Used in qsort() for sorting an array of edges  
int myComp(const void* a, const void* b)
{
    Edge* a1 = (Edge*)a;
    Edge* b1 = (Edge*)b;
    return a1->weight > b1->weight;
}

// The main function to construct MST using Kruskal's algorithm  
vector<pair<Room, Room> > KruskalMST(Graph* graph, vector<Room> rooms)
{
    int V = graph->V;
    vector<Edge> result; // This will store the resultant MST  
    int e = 0; // An index variable, used for result[]  
    int i = 0; // An index variable, used for sorted edges  

    // Step 1: Sort all the edges in non-decreasing  
    // order of their weight. If we are not allowed to  
    // change the given graph, we can create a copy of  
    // array of edges  
    cout << "sort\n";
	sort(graph->edge.begin(), graph->edge.end());

    // Allocate memory for creating V ssubsets  
    subset* subsets = new subset[(V * sizeof(subset))];

    // Create V subsets with single elements  
    for (int v = 0; v < V; ++v)
    {
        subsets[v].parent = v;
        subsets[v].rank = 0;
    }

    // Number of edges to be taken is equal to V-1  
    while (e < V - 1 && i < graph->E)
    {
        // Step 2: Pick the smallest edge. And increment  
        // the index for next iteration  
        Edge next_edge = graph->edge[i++];

        int x = find(subsets, next_edge.src);
        int y = find(subsets, next_edge.dest);

        // If including this edge does't cause cycle,  
        // include it in result and increment the index  
        // of result for next edge  
        if (x != y)
        {
            result.push_back(next_edge);
            e++;
            Union(subsets, x, y);
        }
        // Else discard the next_edge  
    }
    /*for (i = 0; i < graph->E; ++i) {
        cout << graph->edge[i].src << " -- " << graph->edge[i].dest << " == " << graph->edge[i].weight << endl;
    }*/
    vector<pair<Room, Room> > halls;

    // print the contents of result[] to display the  
    // built MST  
    cout << "Following are the edges in the constructed MST\n";
    for (i = 0; i < e; ++i) {
        pair<Room, Room> hall = pair<Room, Room>(rooms[result[i].src], rooms[result[i].dest]);
        halls.push_back(hall);
        //cout << result[i].src << " -- " << result[i].dest << " == " << result[i].weight << endl;
    }
    return halls;
}

// Driver code 
vector<pair<Room, Room> > buildMST(vector<Room> rooms)
{
    int V = rooms.size(); // Number of vertices in graph  
    int E = 0; // Number of edges in graph  
    for (int i = rooms.size() - 1; i > 0; i--)
        E += i;
    Graph* graph = createGraph(V, E);

    for (int i = 0; i < rooms.size(); i++) {
        for (int j = i + 1; j < rooms.size(); j++) {
            Edge e = Edge();
            e.src = i;
            e.dest = j;
            e.weight = rooms[i].getDistance(&*(rooms[j]));
            graph->edge.push_back(e);
        }
    }

    return KruskalMST(graph, rooms);
}

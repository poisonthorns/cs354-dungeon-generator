#include "Translator.h"

using namespace std;

vector<GLfloat> generateGrid(vector<vector<int> > dungeon) {
	GLfloat yincrement = 2.0f / dungeon.size();
	GLfloat xincrement = 2.0f / dungeon[0].size();
	vector<GLfloat> gen;
	for (int i = 0; i < dungeon.size(); i++) {
		for (int j = 0; j < dungeon[0].size(); j++) {
			if (dungeon[i][j] != -1) {
				GLfloat x = 1.0f - xincrement * j;
				GLfloat y = 1.0f - yincrement * i;
				GLfloat x2 = x - xincrement;
				GLfloat y2 = y - yincrement;
				gen.push_back(x); gen.push_back(y); gen.push_back(0.0f);	//top left
				gen.push_back(x2); gen.push_back(y); gen.push_back(0.0f);	//top right
				gen.push_back(x); gen.push_back(y2); gen.push_back(0.0f);	//bottom left
				gen.push_back(x2); gen.push_back(y); gen.push_back(0.0f);	//top right
				gen.push_back(x); gen.push_back(y2); gen.push_back(0.0f);	//bottom left
				gen.push_back(x2); gen.push_back(y2); gen.push_back(0.0f);	//bottom right
			}
		}
	}
	gen.push_back(-1.0f); gen.push_back(-1.0f); gen.push_back(0.0f);
	gen.push_back(-1.0f); gen.push_back(1.0f); gen.push_back(0.0f);
	gen.push_back(1.0f); gen.push_back(1.0f); gen.push_back(0.0f);
	gen.push_back(-1.0f); gen.push_back(-1.0f); gen.push_back(0.0f);
	gen.push_back(1.0f); gen.push_back(1.0f); gen.push_back(0.0f);
	gen.push_back(1.0f); gen.push_back(-1.0f); gen.push_back(0.0f);
	
	return gen;
};

vector<GLfloat> colorGrid(vector<vector<int> > dungeon) {
	cout << "backgroundColor: " << backgroundColor.x << ", " << backgroundColor.y << ", " << backgroundColor.z << endl;
	cout << "roomColor: " << roomColor.x << ", " << roomColor.y << ", " << roomColor.z << endl;
	cout << "hallwayColor: " <<  hallwayColor.x << ", " << hallwayColor.y << ", " << hallwayColor.z << endl << endl;;
	vector<GLfloat> gen;
	for (int i = 0; i < dungeon.size(); i++) {
		for (int j = 0; j < dungeon[0].size(); j++) {
			if (dungeon[i][j] == color::Room) {
				for (int x = 0; x < 6; x++) {
					gen.push_back(roomColor.x); gen.push_back(roomColor.y); gen.push_back(roomColor.z);
				}
			}
			if (dungeon[i][j] == color::Exit) {
				for (int x = 0; x < 6; x++) {
					gen.push_back(1.0f); gen.push_back(0.0f); gen.push_back(0.0f);
				}
			}
			if (dungeon[i][j] == color::Entrance) {
				for (int x = 0; x < 6; x++) {
					gen.push_back(0.0f); gen.push_back(1.0f); gen.push_back(0.0f);
				}
			}
			if (dungeon[i][j] == color::Hallway) {
				for (int x = 0; x < 6; x++) {
					gen.push_back(hallwayColor.x); gen.push_back(hallwayColor.y); gen.push_back(hallwayColor.z);
				}
			}
			if (dungeon[i][j] == color::Trap) {
				for (int x = 0; x < 6; x++) {
					gen.push_back(trapColor.x); gen.push_back(trapColor.y); gen.push_back(trapColor.z);
				}
			}
		}
	}
	for (int x = 0; x < 6; x++) {
		gen.push_back(backgroundColor.x); gen.push_back(backgroundColor.y); gen.push_back(backgroundColor.z);
	}
	return gen;
};

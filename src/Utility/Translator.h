#ifndef TRANSLATOR_H
#define TRANSLATOR_H

#include <iostream>
#include <stdlib.h>
#include <vector>
#include <GL/glew.h>
#include <glm/gtc/matrix_transform.hpp>
#include <common/imgui-1.69/imgui.h>"
#include <common/imgui-1.69/imgui_impl_glfw.h>
#include <common/imgui-1.69/imgui_impl_opengl3.h>

#include "../Config.h"

using namespace glm;
using namespace std;

enum color { Room, Entrance, Exit, Hallway, Trap };

vector<GLfloat> generateGrid(vector<vector<int> > dungeon);
vector<GLfloat> colorGrid(vector<vector<int> > dungeon);

#endif

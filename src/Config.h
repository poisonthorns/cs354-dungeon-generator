#ifndef Config_H
#define Config_H

#include <common/imgui-1.69/imgui.h>"
#include <common/imgui-1.69/imgui_impl_glfw.h>
#include <common/imgui-1.69/imgui_impl_opengl3.h>

extern int GRID_SIZE;
extern float DUNGEON_DENSITY;
extern int MIN_ROOM_SIZE;
extern int MAX_ROOM_SIZE;
extern int TRAP_DENSITY;

extern ImVec4 backgroundColor;
extern ImVec4 roomColor;
extern ImVec4 hallwayColor;
extern ImVec4 trapColor;

#endif

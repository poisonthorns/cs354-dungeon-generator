#ifndef Room_H
#define Room_H

#include "AbstractRoom.h"
#include "RectangularRoom.h"

using namespace std;

class Room
{
	private:
		AbstractRoom* p;
	public:
		pair<int, int> origin_;

		Room(default_random_engine &generator)
		{
			p = new RectangularRoom(generator);
			origin_.first = p->origin_.first;
			origin_.second = p->origin_.second;
		}
		
		Room(default_random_engine &generator, std::pair<int, int> origin)
		{
			p = new RectangularRoom(generator, origin);
			origin_.first = p->origin_.first;
			origin_.second = p->origin_.second;
		}
		
		Room(std::pair<int, int> origin, int sideLength)
		{
			p = new RectangularRoom(origin, sideLength);
			origin_.first = p->origin_.first;
			origin_.second = p->origin_.second;
		}
		
		Room(std::pair<int, int> origin, int width, int height)
		{
			p = new RectangularRoom(origin, width, height);
			origin_.first = p->origin_.first;
			origin_.second = p->origin_.second;
		}
		
		AbstractRoom& operator * ()
		{
			return *p;
		}

		Room(const Room& rhs)
		{
			//come back to this later
			(*this) = rhs;
			origin_.first = rhs.origin_.first;
			origin_.second = rhs.origin_.second;
		}

		~Room()
		{
			delete p;
		}
		
		Room& operator = (const Room& rhs)
		{
			p = rhs.p->clone();
			return *this;
		}
	

		int area()
		{
			return p->area();
		}
		
		void print()
		{
			p->print();
		}
		
		float getDistance(AbstractRoom* secondRoom)
		{
			return p->getDistance(secondRoom);
		}
		
		bool checkOverlap(AbstractRoom* secondRoom)
		{
			return p->checkOverlap(secondRoom);
		}
		
		bool inBounds()
		{
			return p->inBounds();
		}

		void fillDungeon(vector<vector<int> > &dungeon, int type)
		{
			p->fillDungeon(dungeon, type);
		}
		
		pair<float, float> getCenter()
		{
			return p->getCenter();
		}

		int getWidth()
		{
			return p->getWidth();
		}

		int getHeight()
		{
			return p->getHeight();
		}
};

#endif

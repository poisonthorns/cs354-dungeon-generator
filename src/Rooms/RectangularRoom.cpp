#include "RectangularRoom.h"

using namespace std;

bool checkRectangleOverlap(pair<int, int> firstTopLeft, pair<int, int> firstBottomRight, pair<int, int> secondTopLeft, pair<int, int> secondBottomRight) 
{ 
    if(firstTopLeft.first > secondBottomRight.first + 1 || secondTopLeft.first - 1 > firstBottomRight.first) 
        return false; 
  
    if(firstTopLeft.second > secondBottomRight.second + 1 || secondTopLeft.second - 1 > firstBottomRight.second) 
        return false; 
  
    return true; 
}
#ifndef AbstractRoom_H
#define AbstractRoom_H

#include <vector>
#include <utility>

#include "../Config.h"

using namespace std;

class AbstractRoom
{
	public:
		pair <int, int> origin_;
		AbstractRoom() {
			int xCoord = rand() % GRID_SIZE;
			int yCoord = rand() % GRID_SIZE;
			std::pair <int, int> origin (xCoord, yCoord);
			origin_ = origin;
		}
		
		AbstractRoom(std::pair<int, int> origin) {
			origin_ = origin;
		}
		
		AbstractRoom* operator * ()
		{
			return this;
		}

		virtual ~AbstractRoom() = default;

		virtual AbstractRoom* clone () const = 0;

		virtual int area() = 0;
		
		virtual void print() = 0;
		
		virtual float getDistance(AbstractRoom* secondRoom) = 0;
		
		virtual bool checkOverlap(AbstractRoom* secondRoom) = 0;
		
		virtual bool inBounds() = 0;
		
		virtual void fillDungeon(vector<vector<int> > &dungeon, int type) = 0;
		
		virtual pair<float, float> getCenter() = 0;

		virtual int getWidth() = 0;

		virtual int getHeight() = 0;
};

#endif

#ifndef RectangularRoom_H
#define RectangularRoom_H

#include <iostream>
#include <assert.h>

#include "AbstractRoom.h"
#include "../Utility/Dice.h"

using namespace std;

bool checkRectangleOverlap(pair<int, int> firstTopLeft, pair<int, int> firstBottomRight, pair<int, int> secondTopLeft, pair<int, int> secondBottomRight);

class RectangularRoom : public AbstractRoom
{
	protected:
		int width_;
		int height_;

	public:
		RectangularRoom (default_random_engine &generator) :
			AbstractRoom(),
			width_ (genRoomSize(generator)),
			height_ (genRoomSize(generator))
		{}
		
		RectangularRoom(default_random_engine &generator, std::pair<int, int> origin) :
			AbstractRoom(origin),
			width_ (genRoomSize(generator)),
			height_ (genRoomSize(generator))
		{}
		
		RectangularRoom(std::pair<int, int> origin, int sideLength):
			AbstractRoom(origin),
			width_(sideLength),
			height_(sideLength)
		{}
		
		RectangularRoom(std::pair<int, int> origin, int width, int height):
			AbstractRoom(origin),
			width_(width),
			height_(height)
		{}
		
		~RectangularRoom () = default;
		
		RectangularRoom (const RectangularRoom& rhs) = default;

		RectangularRoom* clone () const
		{
			return new RectangularRoom(*this);
		}

		int area()
		{
			return width_ * height_;
		}
			
		void print()
		{
			cout << "coordinates: (" << origin_.first << ", " << origin_.second << ")   " << "height: " << height_ << "   " << "width: " << width_ << endl;
			//cout << height_ << "\n" << width_ << "\n\n";
		}
			
		float getDistance(AbstractRoom* secondRoom)
		{
			pair <float, float> firstCenter = this->getCenter();
			pair <float, float> secondCenter = secondRoom->getCenter();
			
			float centerX = firstCenter.first;
			float centerY = firstCenter.second;
			
			float centerX2 = secondCenter.first;
			float centerY2 = secondCenter.second;
			
			return sqrtf( powf(centerX2 - centerX, 2) + powf(centerY2 - centerY, 2) );
		}
			
		bool checkOverlap(AbstractRoom* rhs)
		{
			RectangularRoom* secondRoom = (RectangularRoom*)rhs;
			int x1 = origin_.first;
			int y1 = origin_.second;
			
			int x2 = x1 + width_;
			int y2 = y1 + height_;
			
			pair<int, int> topLeft(x1, y1);
			pair<int, int> bottomRight(x2, y2);

			pair<int, int> secondTopLeft(secondRoom->origin_.first, secondRoom->origin_.second);
			pair<int, int> secondBottomRight(secondRoom->origin_.first + secondRoom->width_, secondRoom->origin_.second + secondRoom->height_);
			
			if(checkRectangleOverlap(topLeft, bottomRight, secondTopLeft, secondBottomRight))
				return true;
			
			return false;
		}
			
		bool inBounds()
		{
			int x1 = origin_.first;
			int y1 = origin_.second;
			
			int x2 = x1 + width_;
			int y2 = y1 + height_;
			
			pair<int, int> topLeft(x1, y1);
			pair<int, int> bottomRight(x2, y2);
			
			if(topLeft.first <= 0 || GRID_SIZE <= bottomRight.first)
			{
				//cout << "Failed in first inBounds check: " << topLeft.first << " " << bottomRight.first << endl;
				return false;
			}
  
			if(topLeft.second <= 0 || GRID_SIZE <= bottomRight.second)
			{
				//cout <<  "Failed in second inBounds check: " << topLeft.second << " " << bottomRight.second << endl;
				return false;
			}				
  
			return true; 
		}
		
		void fillDungeon(vector<vector<int> > &dungeon, int type)
		{
			int first = origin_.first;
			int second = origin_.second;
			this->print();
			for(int w = 0; w < width_; ++w)
			{
				for(int h = 0; h < height_; ++h)
				{
					int tempFirst = first + w;
					int tempSecond = second + h;
					if (tempFirst < GRID_SIZE && tempSecond < GRID_SIZE)
					{
						dungeon[tempSecond][tempFirst] = type;
					}
					else
					{
						this->print();
						assert(0);
					}
				}
			}
		}
		
		pair<float, float> getCenter()
		{
			float centerX = origin_.first + (width_ / 2.0f);
			float centerY = origin_.second + (height_ / 2.0f);
			pair <float, float> center (centerX, centerY);
			return center;
		}

		int getWidth() {
			return width_;
		}

		int getHeight() {
			return height_;
		}
};

#endif

#include <stdio.h>
#include <stdlib.h>
#include <random>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <common/shader.hpp>

#include <common/imgui-1.69/imgui.h>"
#include <common/imgui-1.69/imgui_impl_glfw.h>
#include <common/imgui-1.69/imgui_impl_opengl3.h>

#include "DunGen.h"
#include "Utility/Translator.h"
#include "Config.h"

ImVec4 backgroundColor = ImVec4(0.0f, 0.0f, 0.0f, 0.0f);
ImVec4 roomColor = ImVec4(1.0f, 1.0f, 1.0f, 0.0f);
ImVec4 hallwayColor = ImVec4(0.0f, 0.0f, 1.0f, 0.0f);
ImVec4 trapColor = ImVec4(1.0f, 0.65f, 0.0f, 0.0f);
int GRID_SIZE = 100;
int MIN_ROOM_SIZE = 2;
int MAX_ROOM_SIZE = 7;
float DUNGEON_DENSITY = 0.2;
int TRAP_DENSITY = 5;

GLFWwindow* window;

using namespace glm;

static bool ShowColorSelection()
{
	if (!ImGui::CollapsingHeader("Color Selection"))
		return false;

	if (ImGui::TreeNode("Room Color Selector"))
	{
		static ImVec4 roomColorPickerColor = ImVec4(1.0f, 1.0f, 1.0f, 0.0f);
		static ImVec4 previousRoomColor = ImVec4(1.0f, 1.0f, 1.0f, 0.0f);
		ImGuiColorEditFlags misc_flags = (0) | (ImGuiColorEditFlags_NoDragDrop) | (0) | (ImGuiColorEditFlags_NoOptions);
		ImGui::Text("Color picker:");
		static ImVec4 room_ref_color_v(1.0f, 1.0f, 1.0f, 0.0f);
		ImGuiColorEditFlags flags = misc_flags;
		flags |= ImGuiColorEditFlags_PickerHueBar;
		flags |= ImGuiColorEditFlags_DisplayHex;
		ImGui::ColorPicker4("MyColor##4", (float*)&roomColorPickerColor, flags, &room_ref_color_v.x);
		ImGui::TreePop();
		if(roomColorPickerColor.x != previousRoomColor.x || roomColorPickerColor.y != previousRoomColor.y || roomColorPickerColor.z != previousRoomColor.z)
		{
			roomColor.x = roomColorPickerColor.x;
			roomColor.y = roomColorPickerColor.y;
			roomColor.z = roomColorPickerColor.z;
			previousRoomColor.x = roomColorPickerColor.x;
			previousRoomColor.y = roomColorPickerColor.y;
			previousRoomColor.z = roomColorPickerColor.z;
			return true;
		}
	}
	if (ImGui::TreeNode("Hallway Color Selector"))
	{
		static ImVec4 hallwayColorPickerColor = ImVec4(0.0f, 0.0f, 1.0f, 0.0f);
		static ImVec4 previousHallwayColor = ImVec4(0.0f, 0.0f, 1.0f, 0.0f);
		ImGuiColorEditFlags misc_flags = (0) | (ImGuiColorEditFlags_NoDragDrop) | (0) | (ImGuiColorEditFlags_NoOptions);
		ImGui::Text("Color picker:");
		static ImVec4 hallway_ref_color_v(0.0f, 0.0f, 1.0f, 0.0f);
		ImGuiColorEditFlags flags = misc_flags;
		flags |= ImGuiColorEditFlags_PickerHueBar;
		flags |= ImGuiColorEditFlags_DisplayHex;
		ImGui::ColorPicker4("MyColor##4", (float*)&hallwayColorPickerColor, flags, &hallway_ref_color_v.x);
		ImGui::TreePop();
		if(hallwayColorPickerColor.x != previousHallwayColor.x || hallwayColorPickerColor.y != previousHallwayColor.y || hallwayColorPickerColor.z != previousHallwayColor.z)
		{
			hallwayColor.x = hallwayColorPickerColor.x;
			hallwayColor.y = hallwayColorPickerColor.y;
			hallwayColor.z = hallwayColorPickerColor.z;
			previousHallwayColor.x = hallwayColorPickerColor.x;
			previousHallwayColor.y = hallwayColorPickerColor.y;
			previousHallwayColor.z = hallwayColorPickerColor.z;
			return true;
		}
	}
	if (ImGui::TreeNode("Background Color Selector"))
	{
		static ImVec4 backgroundColorPickerColor = ImVec4(0.0f, 0.0f, 0.0f, 0.0f);
		static ImVec4 previousBackgroundColor = ImVec4(0.0f, 0.0f, 0.0f, 0.0f);
		ImGuiColorEditFlags misc_flags = (0) | (ImGuiColorEditFlags_NoDragDrop) | (0) | (ImGuiColorEditFlags_NoOptions);
		ImGui::Text("Color picker:");
		static ImVec4 background_ref_color_v(0.0f, 0.0f, 0.0f, 0.0f);
		ImGuiColorEditFlags flags = misc_flags;
		flags |= ImGuiColorEditFlags_PickerHueBar;
		flags |= ImGuiColorEditFlags_DisplayHex;
		ImGui::ColorPicker4("MyColor##4", (float*)&backgroundColorPickerColor, flags, &background_ref_color_v.x);
		ImGui::TreePop();
		if(backgroundColorPickerColor.x != previousBackgroundColor.x || backgroundColorPickerColor.y != previousBackgroundColor.y || backgroundColorPickerColor.z != previousBackgroundColor.z)
		{
			backgroundColor.x = backgroundColorPickerColor.x;
			backgroundColor.y = backgroundColorPickerColor.y;
			backgroundColor.z = backgroundColorPickerColor.z;
			previousBackgroundColor.x = backgroundColorPickerColor.x;
			previousBackgroundColor.y = backgroundColorPickerColor.y;
			previousBackgroundColor.z = backgroundColorPickerColor.z;
			return true;
		}
	}
	if (ImGui::TreeNode("Trap Color Selector"))
	{
		static ImVec4 trapColorPickerColor = ImVec4(1.0f, 0.65f, 0.0f, 0.0f);
		static ImVec4 previousTrapColor = ImVec4(1.0f, 0.65f, 0.0f, 0.0f);
		ImGuiColorEditFlags misc_flags = (0) | (ImGuiColorEditFlags_NoDragDrop) | (0) | (ImGuiColorEditFlags_NoOptions);
		ImGui::Text("Color picker:");
		static ImVec4 trap_ref_color_v(1.0f, 0.65f, 0.0f, 0.0f);
		ImGuiColorEditFlags flags = misc_flags;
		flags |= ImGuiColorEditFlags_PickerHueBar;
		flags |= ImGuiColorEditFlags_DisplayHex;
		ImGui::ColorPicker4("MyColor##4", (float*)&trapColorPickerColor, flags, &trap_ref_color_v.x);
		ImGui::TreePop();
		if (trapColorPickerColor.x != previousTrapColor.x || trapColorPickerColor.y != previousTrapColor.y || trapColorPickerColor.z != previousTrapColor.z)
		{
			trapColor.x = trapColorPickerColor.x;
			trapColor.y = trapColorPickerColor.y;
			trapColor.z = trapColorPickerColor.z;
			previousTrapColor.x = trapColorPickerColor.x;
			previousTrapColor.y = trapColorPickerColor.y;
			previousTrapColor.z = trapColorPickerColor.z;
			return true;
		}
	}
	return false;
}

static void ShowSliders()
{
	if(ImGui::CollapsingHeader("Sliders"))
	{
        ImGui::SliderInt("Grid Size", &GRID_SIZE, 25, 500);
		if(MAX_ROOM_SIZE > GRID_SIZE * 0.3)
		{
			MAX_ROOM_SIZE = GRID_SIZE * 0.3;
		}
		ImGui::Spacing();
		ImGui::Spacing();
		ImGui::SliderFloat("Dungeon Density (DunDen)", &DUNGEON_DENSITY, 0.0f, 0.27f, "ratio = %.2f");
		ImGui::Spacing();
		ImGui::Spacing();
		ImGui::SliderInt("Min Room Size", &MIN_ROOM_SIZE, 2, MAX_ROOM_SIZE);
		ImGui::SliderInt("Max Room Size", &MAX_ROOM_SIZE, MIN_ROOM_SIZE, GRID_SIZE * DUNGEON_DENSITY);
		ImGui::Spacing();
		ImGui::Spacing();
		ImGui::SliderInt("Trap Density (Experimental)", &TRAP_DENSITY, 0, 100);
		
    }
}

int main( void )
{
	// Initialise GLFW
	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		getchar();
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow( 960, 960, "DunGen", NULL, NULL);
	if( window == NULL ){
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
		getchar();
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW
	glewExperimental = GL_TRUE; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS); 

	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// Create and compile our GLSL program from the shaders
	GLuint programID = LoadShaders( "TransformVertexShader.vertexshader", "ColorFragmentShader.fragmentshader" );

	// Get a handle for our "MVP" uniform
	GLuint MatrixID = glGetUniformLocation(programID, "MVP");

	// Projection matrix : 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	glm::mat4 Projection = glm::perspective(glm::radians(45.0f), 4.0f / 4.0f, 0.1f, 100.0f);
	// Camera matrix
	glm::mat4 View       = glm::lookAt(
								glm::vec3(0,0,-2.5), // Camera is at (4,3,-3), in World Space
								glm::vec3(0,0,0), // and looks at the origin
								glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
						   );
	// Model matrix : an identity matrix (model will be at the origin)
	glm::mat4 Model      = glm::mat4(1.0f);
	// Our ModelViewProjection : multiplication of our 3 matrices
	glm::mat4 MVP        = Projection * View * Model; // Remember, matrix multiplication is the other way around

	// Our vertices. Tree consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
	// A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
	vector<vector<int> > dungeonGrid = dungeonGenerator(25,25);
	vector<GLfloat> grid = generateGrid(dungeonGrid);
	int n = grid.size();
	GLfloat *g_vertex_buffer_data = new GLfloat[GRID_SIZE*GRID_SIZE*24];
	for (int i = 0; i < n; i++)
		g_vertex_buffer_data[i] = grid[i];

	GLuint vertexbuffer = 0;
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, GRID_SIZE*GRID_SIZE*24, g_vertex_buffer_data, GL_STATIC_DRAW);
	cout << endl << endl << endl << (GRID_SIZE * GRID_SIZE * 24) << endl << endl << endl;

	vector<GLfloat> color = colorGrid(dungeonGrid);
	// One color for each vertex. They were generated randomly.
	int m = color.size();
	GLfloat *g_color_buffer_data = new GLfloat[GRID_SIZE*GRID_SIZE*24];
	for (int j = 0; j < m; j++)
		g_color_buffer_data[j] = color[j];

	GLuint colorbuffer = 0;
	glGenBuffers(1, &colorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, GRID_SIZE * GRID_SIZE * 24, g_color_buffer_data, GL_STATIC_DRAW);

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO &io = ImGui::GetIO();
	// Setup Platform/Renderer bindings
	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL3_Init("#version 130");
	// Setup Dear ImGui style
	ImGui::StyleColorsDark();
	
	bool rerenderAll = false;
	bool rerenderColor = false;
	int drawNum = GRID_SIZE * GRID_SIZE * 6.0;
	do{
		// Clear the screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		if(rerenderAll)
		{
			glClearColor(backgroundColor.x, backgroundColor.y, backgroundColor.z, 0.0f);
			dungeonGrid.clear();
			dungeonGrid = dungeonGenerator(25,25);
			grid = generateGrid(dungeonGrid);
			g_vertex_buffer_data = new GLfloat[GRID_SIZE*GRID_SIZE*24];
			n = grid.size();
			for (int i = 0; i < n; i++)
				g_vertex_buffer_data[i] = grid[i];

			vertexbuffer = 0;
			glGenBuffers(1, &vertexbuffer);
			glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
			glBufferData(GL_ARRAY_BUFFER, GRID_SIZE * GRID_SIZE * 24, g_vertex_buffer_data, GL_STATIC_DRAW);

			color.clear();
			color = colorGrid(dungeonGrid);
			g_color_buffer_data = new GLfloat[GRID_SIZE*GRID_SIZE*24];
			m = color.size();
			for (int j = 0; j < m; j++)
				g_color_buffer_data[j] = color[j];

			colorbuffer = 0;
			glGenBuffers(1, &colorbuffer);
			glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
			glBufferData(GL_ARRAY_BUFFER, GRID_SIZE * GRID_SIZE * 24, g_color_buffer_data, GL_STATIC_DRAW);
			rerenderAll = false;
		}
		else if(rerenderColor)
		{
			glClearColor(backgroundColor.x, backgroundColor.y, backgroundColor.z, 0.0f);
			color.clear();
			color = colorGrid(dungeonGrid);
			g_color_buffer_data = new GLfloat[GRID_SIZE*GRID_SIZE*24];
			m = color.size();
			for (int j = 0; j < m; j++)
				g_color_buffer_data[j] = color[j];

			colorbuffer = 0;
			glGenBuffers(1, &colorbuffer);
			glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
			glBufferData(GL_ARRAY_BUFFER, GRID_SIZE * GRID_SIZE * 24, g_color_buffer_data, GL_STATIC_DRAW);
			rerenderColor = false;
		}
		

		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();
	
		// Use our shader
		glUseProgram(programID);

		// Send our transformation to the currently bound shader, 
		// in the "MVP" uniform
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(
			0,                  // attribute. No particular reason for 0, but must match the layout in the shader.
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);

		// 2nd attribute buffer : colors
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
		glVertexAttribPointer(
			1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
		);

		// Draw the triangle !
		glDrawArrays(GL_TRIANGLES, 0, drawNum); // 12*3 indices starting at 0 -> 12 triangles

		// render your GUI
		ImGui::Begin("DunGen");
		if(ImGui::Button("Generate new dungeon!"))
		{
			rerenderAll = true;
			drawNum = GRID_SIZE * GRID_SIZE * 6.0;
		}
		ImGui::Text("\tMay need to give it a second");
		ImGui::Spacing();
		ShowSliders();
        rerenderColor = ShowColorSelection();
		ImGui::End();

		// Render dear imgui into screen
		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	} // Check if the ESC key was pressed or the window was closed
	while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
		   glfwWindowShouldClose(window) == 0 );

	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
	
	delete [] g_vertex_buffer_data;
	delete [] g_color_buffer_data;
	
	// Cleanup VBO and shader
	glDeleteBuffers(1, &vertexbuffer);
	glDeleteBuffers(1, &colorbuffer);
	glDeleteProgram(programID);
	glDeleteVertexArrays(1, &VertexArrayID);

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}

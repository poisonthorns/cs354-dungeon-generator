//1. We need to create a middle man, refer to Cell.h
//Make a Rooms folder with seperate files for each Room type: 
//	AbstractRoom(this should be current Room) 
//	RectangularRoom(child of AbstractRoom)
//	Room(middle man, not current room, "Smart Pointer")
//	Make other shaped Rooms (ex. Circle())

//2. Finish Debugging Graph after refactoring of Rooms
//Make a simple test case with 4 rooms and calc by hand to make sure Graph is working correctly

//3. Write MST algorithm

//4. Visualizations (how are we translating pairs of Rooms to hallways)
//Prevent intersections(hallwaysXrooms)
//Drawing hallways between rooms
//Crop of dungeon, mark illegal spaces, if no path is possible in crop don't make a path
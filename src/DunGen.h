#ifndef DUNGEN_H
#define DUNGEN_H

#include <iostream>
#include <stdlib.h>
#include <vector>
#include <GL/glew.h>
#include <glm/gtc/matrix_transform.hpp>

//#include "Rooms/Room.h"
#include "Utility/Graph.h"

using namespace glm;
using namespace std;

vector<vector<int> > dungeonGenerator(int length, int height);
vector<GLfloat> generateGrid(vector<vector<int> > dungeon);
vector<GLfloat> colorGrid(vector<vector<int> > dungeon);
vector<Room> generateRooms();
vector<vector<int> > fillDungeon(vector<Room> rooms);
vector<pair<int, int> > connectRooms(vector<vector<int> > dungeon, vector<pair<Room, Room> > roomPairs);
#endif

 // Include standard headers
#include "DunGen.h"
#include "Config.h"

#include <ctime>
#include <random>
using namespace std;

vector<vector<int> > dungeonGenerator(int length, int height) {
	srand(time(NULL));
	
	vector<Room> rooms = generateRooms();
	vector<vector<int> > dungeon = fillDungeon(rooms);
	
	return dungeon;
}

vector<Room> generateRooms()
{
	default_random_engine generator;
	const int MAX_CAPACITY = (int)(GRID_SIZE * GRID_SIZE * DUNGEON_DENSITY);
	int currCapacity = 0;

	vector<Room> rooms;
	int failsInARow = 0;
	
	while(currCapacity <= MAX_CAPACITY)
	{
		
		Room newRoom(generator);
		bool acceptable = newRoom.inBounds();
		if(failsInARow > 2000)
		{
            currCapacity = MAX_CAPACITY;
		}
		
		if(acceptable)
		{
			for(Room secondRoom : rooms)
			{
				acceptable = !(newRoom.checkOverlap(**secondRoom));
				if(!acceptable)
				{
					break;
				}
			}
			
			if(acceptable)
			{
				rooms.push_back(newRoom);
				failsInARow = 0;
				currCapacity += newRoom.area();
			}
			else
			{
				failsInARow++;
			}
		}
		else
		{
			failsInARow++;
		}
	}
	
	return rooms;
}



vector<vector<int> > fillDungeon(vector<Room> rooms)
{
	//create dungeon template
	vector<vector<int> > dungeon;
	for(int i = 0; i < GRID_SIZE; ++i)
	{
		vector<int> temp;
		temp.assign(GRID_SIZE, -1);
		dungeon.push_back(temp);
	}

	//transfer rooms into dungeon
    Room entrance = rooms[0];
    Room exit = rooms[0];
    for (int i = 0; i < rooms.size(); i++)
    {
        rooms[i].fillDungeon(dungeon, 0);
        float distance = rooms[i].getDistance(&*entrance);
        if (rooms[i].getDistance(&*entrance) > exit.getDistance(&*entrance)) {
            exit = rooms[i];
        }
    }
    entrance.fillDungeon(dungeon, 1);
    exit.fillDungeon(dungeon, 2);

	//generate hallways into dungeon
	vector<pair<Room, Room> > roomPairs = buildMST(rooms);
    cout << "done with MST \n";
    vector<pair<int, int> > halls = connectRooms(dungeon, roomPairs);
    cout << "done finding hall coords \n";
    for (pair<int, int> hall : halls) {
        if (rand() % 100 < TRAP_DENSITY) {
            dungeon[hall.first][hall.second] = 4;
        }
        else {
            dungeon[hall.first][hall.second] = 3;
        }
    }
    cout << "done recording halls \n";
	
	return dungeon;
}

vector<pair<int, int> > connectRooms(vector<vector<int> > dungeon, vector<pair<Room, Room> > roomPairs) {
    vector<pair<int, int> > halls;
    for (pair<Room, Room> rooms : roomPairs) {
        //int begin.first, begin.second, end.first, end.second;
        pair<int, int> begin, end;
        
        Room room1 = rooms.first;
        Room room2 = rooms.second;
        int xDirection;
        int yDirection;
        //cout << "find which faces are furthest from one another \n";
        //if the right face of room1 is to the left of the left face of room2 or
        //if the left face of room1 is to the right of the right face of room2 
        if (room2.origin_.first - (room1.origin_.first + room1.getWidth() - 1) > 3  ||
                room1.origin_.first - (room2.origin_.first + room2.getWidth() - 1) > 3) {
            //cout << "choose best x faces \n";
            //choose best x faces
            if (abs(room1.origin_.first + room1.getWidth() - 1 - room2.origin_.first) < 
                    abs(room1.origin_.first - (room2.origin_.first + room2.getWidth() - 1))) {
                begin.first = room1.origin_.first + room1.getWidth() - 1;
                end.first = room2.origin_.first;
            }
            else {
                begin.first = room1.origin_.first;
                end.first = room2.origin_.first + room2.getWidth();
            }
            //select random y position on x face
            if (room1.getHeight() == 2)
                begin.second = rand() % 2 + room1.origin_.second;
            else
                begin.second = rand() % (room1.getHeight() - 2) + room1.origin_.second + 1;
            if (room2.getHeight() == 2)
                end.second = rand() % 2 + room2.origin_.second;
            else
                end.second = rand() % (room2.getHeight() - 2) + room2.origin_.second + 1;

            xDirection = begin.first < end.first ? 1 : -1;
            yDirection = begin.second < end.second ? 1 : -1;
            //cout << "build hallway X\n";
            int x = begin.first;
            int y = begin.second;
            //halls.push_back(pair<int, int>(y, x));
            while (x != (begin.first + end.first) / 2 && x != end.first) {
                //cout << "current: (" << x << ", " << y << ")   target: (" << end.first << ", " << end.second << ")\n";
                x = x + xDirection;
                if (dungeon[y][x] != -1) {
                    //cout << "first loop break " << x << ", " << y << " ymain\n";
                    break;
                }
                halls.push_back(pair<int, int>(y,x));
            }
            while (x != end.first || y != end.second) {
                while (y != end.second) {
                    //cout << "current: (" << x << ", " << y << ")   target: (" << end.first << ", " << end.second << ")\n";
                    y = y + yDirection;
                    if ((dungeon[y + yDirection][x] == 0 && y + yDirection != end.second))
                        break;
                    halls.push_back(pair<int, int>(y,x));
                }
                while (x != end.first) {
                    //cout << "current: (" << x << ", " << y << ")   target: (" << end.first << ", " << end.second << ")\n";
                    x = x + xDirection;
                    if (dungeon[y][x] != -1)
                        break;
                    halls.push_back(pair<int, int>(y,x));
                }
            }
        }
        //if the bottom face of room1 is higher than the top face of room2 or
        //if the top face of room1 is lower than the bottom face of room2 
        else if (room2.origin_.second - (room1.origin_.second + room1.getHeight() - 1) > 3  ||
                room1.origin_.second - (room2.origin_.second + room2.getHeight() - 1) > 3) {
            //cout << "choose best y faces \n";
            //choose best y faces
            if (abs(room1.origin_.second + room1.getHeight() - 1 - room2.origin_.second) < 
                    abs(room1.origin_.second - (room2.origin_.second + room2.getHeight() - 1))) {
                begin.second = room1.origin_.second + room1.getHeight() - 1;
                end.second = room2.origin_.second;
            }
            else {
                begin.second = room1.origin_.second;
                end.second = room2.origin_.second + room2.getHeight();
            }
            //select random x position on y face
            if (room1.getWidth() == 2)
                begin.first = rand() % 2 + room1.origin_.first;
            else
                begin.first = rand() % (room1.getWidth() - 2) + room1.origin_.first + 1;
            if (room2.getWidth() == 2)
                end.first = rand() % 2 + room2.origin_.first;
            else
                end.first = rand() % (room2.getWidth() - 2) + room2.origin_.first + 1;

            xDirection = begin.first < end.first ? 1 : -1;
            yDirection = begin.second < end.second ? 1 : -1;
            //cout << "build hallway\n";
            //build hallway
            int x = begin.first;
            int y = begin.second;
            while (y != ((begin.second + end.second) / 2) && y != end.second) {
                //cout << "current: (" << x << ", " << y << ")   target: (" << end.first << ", " << end.second << ")\n";
                y = y + yDirection;
                if (dungeon[y][x] != -1) {
                    //cout << "first loop break " << x << ", " << y << " ymain\n";
                    break;
                }
                halls.push_back(pair<int, int>(y,x));
            }
            while (x != end.first || y != end.second) {
                while (x != end.first) {
                    //cout << "current: (" << x << ", " << y << ")   target: (" << end.first << ", " << end.second << ")\n";
                    x = x + xDirection;
                    if ((dungeon[y][x + xDirection] == 0 && x + xDirection != end.first))
                        break;
                    halls.push_back(pair<int, int>(y,x));
                }
                while (y != end.second) {
                    //cout << "current: (" << x << ", " << y << ")   target: (" << end.first << ", " << end.second << ")\n";
                    y = y + yDirection;
                    if (dungeon[y][x] != -1)
                        break;
                    halls.push_back(pair<int, int>(y,x));
                }
            }
        }
        //if any gaps are too small default to center to center hallways
        else {
            begin.first = room1.getCenter().first;
            end.first = room2.getCenter().first;
            begin.second = room1.getCenter().second;
            end.second = room2.getCenter().second;

            xDirection = begin.first < end.first ? 1 : -1;
            yDirection = begin.second < end.second ? 1 : -1;
            //cout << "xDirection: " << xDirection << "   yDirection: " << yDirection << "\n";
            //build halls
            int x = begin.first;
            int y = begin.second;
            if (rand() / 2 > 0) {
                x = x + xDirection;
                if (dungeon[y][x] == -1)
                    halls.push_back(pair<int, int>(y, x));
            }
            if (rand() / 2 > 0) {
                y = y + yDirection;
                if (dungeon[y][x] == -1)
                    halls.push_back(pair<int, int>(y, x));
            }
            xDirection = x < end.first ? 1 : -1;
            yDirection = y < end.second ? 1 : -1;
            if (rand() / 2 > 0) {
                while (x != end.first) {
                    //cout << "current: (" << x << ", " << y << ")   target: (" << end.first << ", " << end.second << ")\n";
                    x = x + xDirection;
                    if (dungeon[y][x] == -1)
                        halls.push_back(pair<int, int>(y, x));
                }
            }
            while (y != end.second) {
                //cout << "current: (" << x << ", " << y << ")   target: (" << end.first << ", " << end.second << ")\n";
                y = y + yDirection;
                if (dungeon[y][x] == -1)
                    halls.push_back(pair<int, int>(y, x));
            }
            while (x != end.first) {
                //cout << "current: (" << x << ", " << y << ")   target: (" << end.first << ", " << end.second << ")\n";
                x = x + xDirection;
                if (dungeon[y][x] == -1)
                    halls.push_back(pair<int, int>(y, x));
            }
        }
    }
    return halls;
}
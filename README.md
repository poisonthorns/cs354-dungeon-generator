# CS354 Dungeon Generator  

Partner 1:  
    Name: Jordan Bogaards  
    eid: jpb3484  
Partner 2:  
    Name: Skylore Evans  
    eid: sme2244  
  
Link to our gitlab:
https://gitlab.com/poisonthorns/cs354-dungeon-generator

We used the build instruction from skinning as a basis for this.
Our base in the main file is a modified version of an openGL tutorial.
We used the ImGUI demo for examples in building our GUI.

***Windows Instructions***

1. Download and install Microsoft Visual Studio (with C++ Compiler)
and CMake (https://cmake.org/download/).

2. Clone or Download VCPKG from (https://github.com/Microsoft/vcpkg)

3. Follow the install instruction for VCPKG at
(https://github.com/Microsoft/vcpkg). In particular you need to
perform the following steps:
3a. Open up a command prompt *in administrator mode* (click the
Windows icon, type "cmd", then right-click on "Command Prompt" and
click "Run as administrator").
3b. Navigate to the folder where you extracted VCPKG.
3c. Run the following commands:
.\bootstrap-vcpkg.bat
.\vcpkg integrate install

4. Install the project dependencies:
.\vcpkg install glfw3
.\vcpkg install glew
.\vcpkg install glm
.\vcpkg install zlib
.\vcpkg install libjpeg-turbo

5. Add VCPKG_ROOT to your environment variables. Click the Windows
icon, type "env", and click on "Edit the system environment
variables". Click the "Environment Variables" button. Under "User
variables," click "New..." and add:
Variable Name: VCPKG_ROOT
Value: the path to your VCPKG folder.

6. Open CMake. Set the paths for the starter code:
Where is the source code: /path/to/cs354-dungeon-generator
Where to build the binaries: /path/to/cs354-dungeon-generator/build

7. Hit "Configure". Pick the native compilers and Win32 as the compilation platform (*not* x64). You should not get any errors. Keep hitting
"Configure" until the red lines are all white.

8. Hit "Generate".

9. Hit "Open Project". This will launch Visual Studio with your project open.

10. Right click on dungeon-generator in solution explorer and set as Startup Project
